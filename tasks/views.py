from django.shortcuts import render, redirect
from .forms import TaskForm
from django.contrib.auth.decorators import login_required
from .models import Task


@login_required(login_url="login")
def create_task(request):
    if request.method == "POST":
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            task_form.save()

            return redirect("list_projects")

    else:
        form = TaskForm

    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required(login_url="login")
def show_my_tasks(request):
    task_objects = Task.objects.filter(assignee=request.user)
    context = {"task_objects": task_objects}
    return render(request, "tasks/show_my_tasks.html", context)
