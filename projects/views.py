from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


@login_required(login_url="login")
def list_projects(request):
    list_object = Project.objects.filter(owner=request.user)
    context = {"list_object": list_object}

    return render(request, "projects/project_list.html", context)


@login_required(login_url="login")
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {
        "show_project": show_project,
    }

    return render(request, "projects/show_project.html", context)


@login_required(login_url="login")
def create_project(request):
    if request.method == "POST":
        project_form = ProjectForm(request.POST)
        if project_form.is_valid():
            project_form.save()

            return redirect(list_projects)
    else:
        form = ProjectForm
    context = {"form": form}

    return render(request, "projects/create_project.html", context)
